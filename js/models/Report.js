function Report(id, reportDescription, reportedSprintId, reportStartDate, reportEndDate, reportProjectID,reportTasks) {
    this.id = id;
    this.reportDescription = reportDescription;
    this.reportedSprintsId = reportedSprintId;
    this.reportStartDate = reportStartDate;
    this.reportEndDate = reportEndDate;
    this.projectID = reportProjectID;
    this.reportTasks = reportTasks;
}
var reports = [];