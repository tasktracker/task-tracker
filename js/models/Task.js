function Task(id, sprint, name, user, description, labels, projectId, estimate) {
    this.id = id;
    this.sprintID = sprint;
    this.name = name;
    this.user = user;
    this.description = description;
    this.state = "To Do";
    this.labels = labels;
    this.projectId = projectId;
    this.estimate = estimate;
}
var tasks = [
    {
        "id": 0,
        "sprintID": null,
        "name": "Read documentation",
        "user": 8,
        "description": "Open smth",
        "state": "Completed",
        "labels": ["bug"],
        "projectId": 1,
        "estimate": "1d"
    },
    {
        "id": 1,
        "sprintID": 0,
        "name": "Read the specification",
        "user": 4,
        "description": "Open the word file with instructions",
        "state": "Completed",
        "labels": ["bug", "testing"],
        "projectId": 0,
        "estimate": "1h"
    },
    {
        "id": 2,
        "sprintID": 0,
        "name": "Fill the gaps",
        "user": 1,
        "description": "find the gaps in code and fill them",
        "state": "In Progress",
        "labels": ["new feature"],
        "projectId": 0,
        "estimate": "2h"
    },
    {
        "id": 3,
        "sprintID": 0,
        "name": "Creating a beautiful design",
        "user": 2,
        "description": "surf the internet and look through modern designs",
        "state": "On Review",
        "labels": ["testing"],
        "projectId": 0,
        "estimate": "8h"
    },
    {
        "id": 4,
        "sprintID": 2,
        "name": "Help others",
        "user": 1,
        "description": "Ask if team needs help and start do it if they need",
        "state": "Completed",
        "labels": ["bug"],
        "projectId": 0,
        "estimate": "7h"
    },

    {
        "id": 5,
        "sprintID": 1,
        "name": "Meet the Project manager",
        "user": 6,
        "description": "Tell the PM all the things you have problem with",
        "state": "To Do",
        "labels": ["bug", "new feature"],
        "projectId": 0,
        "estimate": "5h"
    },

    {
        "id": 6,
        "sprintID": 1,
        "name": "Design the User Interface",
        "user": 2,
        "description": "search information about UI Development with Bootstrap",
        "state": "In Progress",
        "labels": ["bug"],
        "projectId": 0,
        "estimate": "1d"
    },
    {
        "id": 7,
        "sprintID": 2,
        "name": "Add navigation",
        "user": 5,
        "description": "Add navigation bar and div blocks for tabs",
        "state": "On Review",
        "labels": ["testing"],
        "projectId": 0,
        "estimate": "8h"

    },
    {
        "id": 8,
        "sprintID": 2,
        "name": "Add grid",
        "user": 4,
        "description": "Add grid to sprintID board tab",
        "state": "In Progress",
        "labels": ["bug"],
        "projectId": 0,
        "estimate": "4h"
    },
    {
        "id": 9,
        "sprintID": 2,
        "name": "Add labels",
        "user": 3,
        "description": "Add labels to tasks in <Backlog> tab",
        "state": "In Progress",
        "labels": ["testing"],
        "projectId": 0,
        "estimate": "6h"
    },

    {
        "id": 10,
        "sprintID": null,
        "name": "External files",
        "user": 5,
        "description": "put big divs from index file to external files and connect it to index (ng-include)",
        "state": "To Do",
        "labels": ["bug"],
        "projectId": 0,
        "estimate": "8h"
    },
    {
        "id": 11,
        "sprintID": null,
        "name": "Json file",
        "user": 1,
        "description": "Creating and filling json file",
        "state": "In Progress",
        "labels": ["testing"],
        "projectId": 0,
        "estimate": "2h"
    },
    {
        "id": 12,
        "sprintID": null,
        "name": "Presentation",
        "user": 3,
        "description": "Show all the work to others",
        "state": "On Review",
        "labels": ["new feature"],
        "projectId": 0,
        "estimate": "9h"
    },
    {
        "id": 13,
        "sprintID": 3,
        "name": "UI Develop",
        "user": 8,
        "description": "Working Hard",
        "state": "To Do",
        "labels": ["new feature", "bug"],
        "projectId": 1,
        "estimate": "6h"
    },
    {
        "id": 14,
        "sprintID": 3,
        "name": "Bootstrap Dev",
        "user": 7,
        "description": "Working Hard",
        "state": "In Progress",
        "labels": ["new feature", "testing"],
        "projectId": 1,
        "estimate": "5h"
    }
];