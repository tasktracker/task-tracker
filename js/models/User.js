function User(id, name, email, nick) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.nick = nick;
    this.projectId = [];
}
var users = [

    {
        "id": 0,
        "name": "Melissa",
        "nick": "melis",
        "email": "melis@gmail.com",
        "projectId": [0]
    },
    {
        "id": 1,
        "name": "John",
        "nick": "goodboy",
        "email": "good@gmail.com",
        "projectId": [0]
    },
    {
        "id": 2,
        "name": "Tom",
        "nick": "badboy",
        "email": "bad@gmail.com",
        "projectId": [0]
    },
    {
        "id": 3,
        "name": "Jerry",
        "nick": "coolboy",
        "email": "cool@gmail.com",
        "projectId": [0]
    },
    {
        "id": 4,
        "name": "Alicia",
        "nick": "coolgirl",
        "email": "coolgirl@gmail.com",
        "projectId": [0]
    },
    {
        "id": 5,
        "name": "Jerom",
        "nick": "notboy",
        "email": "notboy@gmail.com",
        "projectId": [0]
    },
    {
        "id": 6,
        "name": "Niki",
        "nick": "bautiguru",
        "email": "bautiguru@gmail.com",
        "projectId": [0]
    },
    {
        "id": 7,
        "name": "Bob",
        "nick": "bobanchyk",
        "email": "boba@gmail.com",
        "projectId": [1]
    },
    {
        "id": 8,
        "name": "Petrovich",
        "nick": "petrovychpetya",
        "email": "petro@gmail.com",
        "projectId": [1]
    },
    {
        "id": 9,
        "name": "Ivanovich",
        "nick": "Ivanovichenko",
        "email": "Ivan@gmail.com",
        "projectId": []
    },
    {
        "id": 10,
        "name": "Vasa",
        "nick": "Vasulko",
        "email": "Vasa@gmail.com",
        "projectId": []
    }
];