function Project(id, name, description) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.isFinished = false;
}
var projects = [
    {
        "id": 0,
        "name": "Project 1",
        "description": "This is Project 1",
        "isFinished": false
    },
    {
        "id": 1,
        "name": "Project 2",
        "description": "This is Project 2",
        "isFinished": false
    }
];