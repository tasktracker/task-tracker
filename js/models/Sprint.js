function Sprint(id, startDate, endDate, projectId) {
    this.id = id;
    this.startDate = startDate;
    this.endDate = endDate;
    this.projectId = projectId;
}
var sprints = [
    {
        "id": 0,
        "startDate": 1459162032000,
        "endDate": 1459782032000,
        "projectId": 0
    },
    {
        "id": 1,
        "startDate": 1457862032001,
        "endDate": 1445782032001,
        "projectId": 0
    },
    {
        "id": 2,
        "startDate": 1447777032002,
        "endDate": 1459162032002,
        "projectId": 0
    },
    {
        "id": 3,
        "startDate": 1459999932003,
        "endDate": 1487462032003,
        "projectId": 1
    }
];