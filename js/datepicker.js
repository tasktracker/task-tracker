$(function () {
    $(".from").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        changeYear: true,
        onClose: function (selectedDate) {
            $(".to").datepicker("option", "minDate", selectedDate);
        }
    });
    $(".to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        changeYear: true,
        onClose: function (selectedDate) {
            $(".from").datepicker("option", "maxDate", selectedDate);
        }
    });

    function toggleChevron(e) {
        $(e.target) 
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('glyphicon-triangle-bottom glyphicon-triangle-top');
    }

    $('.accordion').on('hidden.bs.collapse', toggleChevron);
    $('.accordion').on('shown.bs.collapse', toggleChevron);

});




