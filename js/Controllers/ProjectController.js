angular.module('tttApp').controller('projectCtrl', function ($scope) {
    $scope.setNewProjectObject = function () {
        $scope.newProject = {
            "name": null,
            "description": null,
            "usersId": null
        };
    };

    $scope.addNewProject = function (name, description, form) {
        var newProject = new Project($scope.projects.length, name, description);
        $scope.projects.push(newProject);
        $scope.updateLocalStorage('projects');
        form.$setPristine();
        $.simplyToast('New project successfully added', 'success');
    };

    $scope.setProjectEditFields = function (id) {
        $scope.selectionUsers = [];
        $scope.editedProject = $scope.createCopyOfObjectForEdit(id, $scope.projects);
    };

    $scope.saveProjectEditChanges = function () {
        $scope.projects[$scope.editID] = angular.copy($scope.editedProject);
        for (var i = 0; i < $scope.selectionUsers.length; i++) {
            $scope.users[$scope.selectionUsers[i]].projectId.push($scope.editID);
        }
        $scope.updateLocalStorage("users");
        $scope.updateLocalStorage('projects');
        $.simplyToast('Changes saved', 'info');
    };

    $scope.setNewUserObject = function () {
        $scope.newUser = {
            "name": null,
            "nick": null,
            "email": null
        };
    };
    $scope.addNewUser = function (name, email, nick, form) {
        var newUser = new User($scope.users.length, name, email, nick);
        $scope.users.push(newUser);
        $scope.updateLocalStorage('projects');
        form.$setPristine();
        $.simplyToast('New user successfully added', 'success');
    };

    $scope.selectionUsers = [];
    $scope.toggleSelectionUsers = function (checkedUser) {
        var idx = $scope.selectionUsers.indexOf(checkedUser);
        if (idx > -1) {
            $scope.selectionUsers.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selectionUsers.push(checkedUser);
        }
    };

    $scope.checkUsersWithoutProjects = function () {
        var checked = true;
        for (var i = 0; i < $scope.users.length; i++) {
            if (typeof $scope.users[i].projectId[0] === "undefined") {
                checked = false;
            }
        }
        return checked;
    };

    $scope.showUsersThatAreNotAttachedToThisProject = function (userId, projID) {
        var show = false;
        for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].id === userId) {
                if (typeof $scope.users[i].projectId[0] === "undefined")
                    show = true;
                for (var j = 0; j < $scope.users[i].projectId.length; j++) {
                    if ($scope.users[i].projectId[j] !== projID) {
                        show= true;
                    }
                }
            }
        }
        return show;
    }

});