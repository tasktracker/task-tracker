angular.module('tttApp').controller('ChartCtrl', function ($scope, $rootScope) {

    $rootScope.buildCharts = function () {
        $scope.generateDataForCharts();

        var sprintsAndTasksChart = c3.generate({
            bindto: '#sprintsAndTasks',
            data: {
                x: 'x',
                columns: $scope.dataForFirstChart,
                type: 'bar',
                colors: {
                    'To Do': '#bf00ff',
                    'In Progress': '#0080ff',
                    'On Review': '#ff8000',
                    'Completed': '#00ff00'
                }
            },
            axis: {
                x: {
                    label: 'Sprints',
                    type: 'category',
                    tick: {
                        rotate: 0,
                        multiline: true
                    },
                    height: 50
                },
                y: {
                    tick: {
                        format: d3.format("d")
                    },
                    label: 'Number of tasks'
                }
            }
        });

        var tasksByStateChart = c3.generate({
            bindto: '#tasksByState',
            data: {
                columns: $scope.dataForSecondChart,
                type: 'pie',
                colors: {
                    'To Do': '#bf00ff',
                    'In Progress': '#0080ff',
                    'On Review': '#ff8000',
                    'Completed': '#00ff00'
                },
                onclick: function (d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function (d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function (d, i) {
                    console.log("onmouseout", d, i);
                }
            }
        });

        var tasksByUsersChart = c3.generate({
            bindto: '#tasksByUsers',
            data: {
                columns: $scope.dataForThirdChart,
                type: 'donut'
            }

        });
    };
});
