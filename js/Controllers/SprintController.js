angular.module('tttApp').controller('sprintCtrl', function ($scope, $filter) {
    $scope.setNewSprintObject = function () {
        //empty object for new sprint modal form
        $scope.newSprint = {
            "startDate": null,
            "endDate": null
        };
    };
    $scope.addNewSprint = function (startDate, endDate, form) {
        var newSprintId = $scope.sprints.length;
        var newSprint = new Sprint(newSprintId, startDate, endDate, $scope.currentProjectID);
        for (var j = 0; j < $scope.selection.length; j++) {
            $scope.tasks[$scope.selection[j]].sprintID = newSprintId;
        }
        // $scope.tasks[x].sprintID = newSprintId;
        $scope.sprints.push(newSprint);
        $scope.updateSprints($scope.currentProjectID);
        $scope.updateTasks($scope.currentProjectID);
        //push new sprint ID to sprintID array in project
        if ($scope.projects[$scope.currentProjectID].sprintsId)
            $scope.projects[$scope.currentProjectID].sprintsId.push(newSprintId);
        else
            $scope.projects[$scope.currentProjectID].sprintsId = [newSprintId];
        $scope.selection = [];
        $scope.updateLocalStorage('tasks');
        $scope.updateLocalStorage('sprints');
        $scope.updateLocalStorage('projects');
        //clear form
        $scope.selection = [];
        form.$setPristine();
        form.$setUntouched();
        //clear date picker start/end date
        $(".from").datepicker("option", {
            minDate: null,
            maxDate: null
        });
        $(".to").datepicker("option", {
            minDate: null,
            maxDate: null
        });
        $.simplyToast('New sprint successfully added', 'success');
        //end of clear form
    };

    $scope.saveSprintEditChanges = function () {
        $scope.sprints[$scope.editID] = angular.copy($scope.editedSprint);
        $scope.updateSprints($scope.currentProjectID);
        $scope.updateLocalStorage('sprints');
        $.simplyToast('Changes saved', 'info');
    };

    $scope.setSprintEditFields = function (id) {
        $scope.editedSprint = $scope.createCopyOfObjectForEdit(id, $scope.sprints);
        //date types convert
        $scope.editedSprint.startDate = $filter('date')($scope.editedSprint.startDate, 'MM/dd/yyyy');
        $scope.editedSprint.endDate = $filter('date')($scope.editedSprint.endDate, 'MM/dd/yyyy');
    };

    $scope.setReportData = function (sprint) {
        var reportTasks = [];
        for (var i = 0; i < $scope.projectTasks.length; i++) {
            if ($scope.projectTasks[i].sprintID === sprint.id) {
                if ($scope.projectTasks[i].state === 'Completed') {
                    reportTasks.push(angular.copy($scope.projectTasks[i]));
                }
            }
        }
        $scope.newReport = {
            "reportDescription": null,
            "reportedSprintsId": sprint.id,
            "reportStartDate": sprint.startDate,
            "reportEndDate": sprint.endDate,
            "reportProjectID": sprint.projectId,
            "reportTasks": reportTasks
        };
    };

    $scope.checkIfCompletedTaskExist = function (reportedSprintId) {
        var currentTasks = [];
        var currentSprint = null;
        var sprints = $scope.sprints;
        for (var i = 0; i < sprints.length; i++) {
            if (sprints[i].id === reportedSprintId) {
                currentSprint = sprints[i];
            }
        }
        if (currentSprint == null) {
            return false;
        }

        for (var i = 0; i < $scope.projectTasks.length; i++) {
            if ($scope.projectTasks[i].sprintID === currentSprint.id)
                currentTasks.push($scope.projectTasks[i]);
        }

        for (var i = 0; i < currentTasks.length; i++) {
            if (currentTasks[i].state === "Completed")
                return true;
        }

        return false;
    };

    $scope.selection = [];
    $scope.toggleSelection = function (checkedTask) {
        var idx = $scope.selection.indexOf(checkedTask);
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

        else {
            $scope.selection.push(checkedTask);
        }
    };
    //delete methods
    $scope.setDeleteSprint = function (id) {
        $scope.sprintToDeleteId = id;
        $scope.deletedSprint = $scope.createCopyOfObjectForEdit(id, $scope.sprints);
    };

    $scope.confirmDeletedWithTasks = function () {
        $scope.sprints.splice($scope.sprintToDeleteId, 1);
        for (var i = 0; i < $scope.tasks.length; i++) {
            if ($scope.sprintToDeleteId === $scope.tasks[i].sprintID) {
                $scope.tasks.splice(i, 1);
                i--;
            }
        }
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateSprints($scope.currentProjectID);
        $scope.updateLocalStorage("tasks");
        $scope.updateLocalStorage("sprints");
        $.simplyToast('Sprint with tasks deleted', 'danger');

    };

    $scope.confirmDeletedWithoutTasks = function () {

        $scope.sprints.splice($scope.sprintToDeleteId, 1);
        for (var i = 0; i < $scope.tasks.length; i++) {
            if ($scope.sprintToDeleteId === $scope.tasks[i].sprintID) {
                $scope.tasks[i].sprintID = null;
            }
        }
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateSprints($scope.currentProjectID);
        $scope.updateLocalStorage("tasks");
        $scope.updateLocalStorage("sprints");
        $.simplyToast('Sprint deleted', 'danger');
    };

    $scope.checkSprintsWithoutTasks = function () {
        var checked = true;
        for (var i = 0; i < $scope.projectTasks.length; i++) {
            if ($scope.projectTasks[i].sprintID === null) {
                checked = false;
            }
        }
        return checked;
    };
});
