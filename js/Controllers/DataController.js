angular.module('tttApp').controller('dataCtrl', function ($scope, $localStorage, $filter) {
    $scope.setCurrentPage = function () {
        $scope.projectPage = !$scope.projectPage;
    };
    $scope.setActiveTab = function (tab) {
        $scope.activeTab = tab;
    };

    $scope.loadData = function () {
        $scope.loadFromLocalStorage();
        $scope.setCurrentPage();
        $scope.projectPage = true;
        $scope.projectTasks = [];
        $scope.reportedTasks = [];
        $scope.projectSprints = [];
    };

    $scope.loadFromLocalStorage = function () {
        $scope.projects = $localStorage.projects || projects;
        $scope.sprints = $localStorage.sprints || sprints;
        $scope.tasks = $localStorage.tasks || tasks;
        $scope.users = $localStorage.users || users;
        $scope.labels = $localStorage.labels || labelsModel;
        $scope.taskGroup = $localStorage.taskGroup || groups;
        $scope.reports = $localStorage.reports || reports;
        $scope.reportedTasks = $localStorage.reportedTasks;
    };

    $scope.updateLocalStorage = function (itemToUpdate) {
        switch (itemToUpdate) {
            case 'projects':
                $localStorage.projects = $scope.projects;
                break;
            case 'sprints':
                $localStorage.sprints = $scope.sprints;
                break;
            case 'tasks':
                $localStorage.tasks = $scope.tasks;
                break;
            case 'users':
                $localStorage.users = $scope.users;
                break;
            case 'labels':
                $localStorage.labels = $scope.labels;
                break;
            case 'reports':
                $localStorage.reports = $scope.reports;
                break;
            case 'reportedTasks':
                $localStorage.reportedTasks = $scope.reportedTasks;
                break;
            case 'taskGroup':
                $localStorage.taskGroup = $scope.taskGroup;
                break;
            default:
                alert('Update fail');
                break;
        }
    };

    $scope.loadProject = function (projID) {
        $scope.currentProjectID = projID;
        $scope.setCurrentPage();
        $scope.updateTasks(projID);
        $scope.updateSprints(projID);
        $scope.updateUsers(projID);
        $scope.setActiveTab(1);
        $scope.currentSprint = $scope.projectSprints.length - 1;
    };

    //update methods
    $scope.updateTasks = function (projID) {
        for (var i = 0; i < $scope.tasks.length; i++) {
            if (!($scope.tasks[i].id === i)) {
                $scope.tasks[i].id = i;
            }
        }
        $scope.projectTasks = [];
        $scope.tasksWithoutSprint = [];
        $scope.tasksSortedByState = [[], [], [], []];
        for (var x in $scope.tasks) {
            if ($scope.tasks[x].projectId === projID) {
                $scope.projectTasks.push($scope.tasks[x]);
                //check if sprintID = null
                if ($scope.tasks[x].sprintID === null) $scope.tasksWithoutSprint.push($scope.tasks[x]);
                switch ($scope.tasks[x].state) {
                    case "To Do":
                        $scope.tasksSortedByState[0].push($scope.tasks[x]);
                        break;
                    case "In Progress":
                        $scope.tasksSortedByState[1].push($scope.tasks[x]);
                        break;
                    case "On Review":
                        $scope.tasksSortedByState[2].push($scope.tasks[x]);
                        break;
                    case "Completed":
                        $scope.tasksSortedByState[3].push($scope.tasks[x]);
                        break;
                }
            }
        }
    };

    $scope.updateTasksFromTaskGroups = function () {
        for (var i = 0; i < $scope.tasksSortedByState.length; i++) {
            for (var j = 0; j < $scope.tasksSortedByState[i].length; j++) {
               for (var k=0; k<$scope.tasks.length; k++) {
                   if($scope.tasks.id===$scope.tasksSortedByState[i][j].id) {
                       $scope.tasks[k]= $scope.tasksSortedByState[i][j];
                   }
               }
            }
        }
    };

    $scope.updateSprints = function (projID) {
        $scope.projectSprints = [];
        for (var i = 0; i < $scope.sprints.length; i++) {
            if (!($scope.sprints[i].id === i)) {
                for (var j = 0; j < $scope.tasks.length; j++) {
                    if ($scope.sprints[i].id === $scope.tasks[j].sprintID) {
                        $scope.tasks[j].sprintID = i
                    }
                }
                $scope.sprints[i].id = i;
                // alert("moved"+i);
            }
        }
        for (var k = 0; k < $scope.sprints.length; k++) {
            if ($scope.sprints[k].projectId === projID) {
                $scope.projectSprints.push($scope.sprints[k]);
            }
        }
    };

    $scope.updateUsers = function (projID) {
        for (var k = 0; k < $scope.users.length; k++) {
            if (!($scope.users[k].id === k)) {
                for (var h = 0; h < $scope.tasks.length; h++) {
                    if ($scope.users[k].id === $scope.tasks[h].user) {
                        $scope.tasks[h].user = k;
                    }
                }
                $scope.users[k].id = k;
            }
        }
        $scope.projectUsers = [];
        for (var i = 0; i < $scope.users.length; i++) {
            for (var j=0; j<$scope.users[i].projectId.length;j++) {
                if ($scope.users[i].projectId[j] === projID) {
                    $scope.projectUsers.push($scope.users[i]);
                }
            }
        }
    };
    //end of update methods

    $scope.getObjectNameById = function (id, array) {
        var name;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === id) {
                name = array[i].name;
            }
        }
        return name;
    };

    $scope.generateSprintName = function (id) {
        var sprint;
        for (var i = 0; i < $scope.projectSprints.length; i++) {
            if ($scope.projectSprints[i].id === id)
                sprint = $scope.projectSprints[i];
        }
        return 'Sprint : (' + $filter('date')(sprint.startDate, 'MM/dd/yyyy') + ' - ' + $filter('date')(sprint.endDate, 'MM/dd/yyyy') + ')';
    };

    $scope.generateDataForCharts = function () {
        //data for first chart
        $scope.dataForFirstChart = [['x'], ['To Do'], ['In Progress'], ['On Review'], ['Completed']];
        for (var i = 0; i < $scope.projectSprints.length; i++) {
            //xAxis for first chart
            var name = $scope.generateSprintName($scope.projectSprints[i].id);
            $scope.dataForFirstChart[0].push(name);
            //number of tasks by sprint
            var numberOfTasks = [0, 0, 0, 0];
            for (var j = 0; j < $scope.projectTasks.length; j++) {
                if ($scope.projectTasks[j].sprintID === $scope.projectSprints[i].id) {
                    switch ($scope.projectTasks[j].state) {
                        case "To Do":
                            numberOfTasks[0]++;
                            break;
                        case "In Progress":
                            numberOfTasks[1]++;
                            break;
                        case "On Review":
                            numberOfTasks[2]++;
                            break;
                        case "Completed":
                            numberOfTasks[3]++;
                            break;
                    }
                }
            }
            //push number of tasks (sorted by state) to chart data
            for (var y = 0; y < numberOfTasks.length; y++)
                $scope.dataForFirstChart[y + 1].push(numberOfTasks[y])
        }

        //data for second chart
        $scope.dataForSecondChart = [["To Do", 0], ["In Progress", 0], ["On Review", 0], ["Completed", 0]];
        for (var x in $scope.projectTasks) {
            switch ($scope.projectTasks[x].state) {
                case "To Do":
                    $scope.dataForSecondChart[0][1]++;
                    break;
                case "In Progress":
                    $scope.dataForSecondChart[1][1]++;
                    break;
                case "On Review":
                    $scope.dataForSecondChart[2][1]++;
                    break;
                case "Completed":
                    $scope.dataForSecondChart[3][1]++;
                    break;
            }
        }
        //data for third chart

        $scope.dataForThirdChart = [];
        for (var m = 0; m < $scope.projectUsers.length; m++) {
            $scope.dataForThirdChart.push([]);
            $scope.dataForThirdChart[m].push($scope.projectUsers[m].name);
            //set number of tasks = 0;
            $scope.dataForThirdChart[m].push(0);
            for (var u = 0; u < $scope.projectTasks.length; u++) {
                if ($scope.projectTasks[u].user === $scope.projectUsers[m].id) {
                    $scope.dataForThirdChart[m][1]++;
                }
            }
        }

    };

    //filters
    $scope.setFilterByUser = function (filter) {
        if (filter === '' || filter === null || filter === undefined)
            return filter = undefined;
        return parseInt(filter);
    };

    $scope.setFilterReturnString = function (filter) {
        if (filter === '' || filter === null || filter === undefined)
            return filter = undefined;
        return filter;
    };

    $scope.resetLocalStorage = function () {
        $localStorage.$reset();
    };
    //create copy of some object for add modal forms
    $scope.createCopyOfObjectForEdit = function (id, array) {
        $scope.editID = id;
        return angular.copy(array[id]);
    };
    //drag'n'drop
    $scope.sortableOptions = {
        cursor: 'move',
        placeholder: "task-dnd",
        connectWith: ".task-container",
        /* fix scroll bug in chrome */
        appendTo: 'body',
        scroll: false,
        helper: 'clone',
        /* end of fix */
        stop: function (event, ui) {
            for (var i = 0; i < $scope.tasksSortedByState.length; i++) {
                for (var j = 0; j < $scope.tasksSortedByState[i].length; j++) {
                    $scope.tasksSortedByState[i][j].state = $scope.taskGroup[i];
                    $scope.updateTasksFromTaskGroups();
                }
            }
            $scope.updateTasks($scope.currentProjectID);
            $scope.updateLocalStorage('tasks');
        }
    };

    $scope.hideSprintsWithoutTasksAfterFilter = function (sprintid, filterByUser) {
        var show = true;
        if (filterByUser===undefined) 
            return false;
        else  {
            for (var i=0; i<$scope.projectTasks.length; i++) {
                if($scope.projectTasks[i].sprintID===sprintid) {
                    if ($scope.projectTasks[i].user === filterByUser) {
                        show = false;
                    }
                }
            }
        }
        return show;
    }
});