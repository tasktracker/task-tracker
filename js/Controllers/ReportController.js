angular.module('tttApp').controller('reportCtrl', function ($scope, $rootScope) {
    $scope.addNewReport = function () {
        var newReport = new Report($scope.reports.length, $scope.newReport.reportDescription, $scope.newReport.reportedSprintsId, $scope.newReport.reportStartDate, $scope.newReport.reportEndDate, $scope.newReport.reportProjectID, $scope.newReport.reportTasks);
        var sprintId = newReport.reportedSprintsId;
        if (!this.checkIfExists(sprintId)) {
            this.replaceReports(sprintId, newReport);
            $.simplyToast('Report successfully modified', 'success')
        }
        else {
            $scope.reports.push(newReport);
            $.simplyToast('New report successfully added', 'success');
        }

        $scope.updateLocalStorage('reports');
    };

    $scope.checkIfExists = function (id) {
        for (var i = 0; i < $scope.reports.length; i++) {
            if ($scope.reports[i].reportedSprintsId === id)
                return false;
        }
        return true;
    };

    $scope.replaceReports = function (id, report) {
        for (var i = 0; i < $scope.reports.length; i++) {
            if ($scope.reports[i].reportedSprintsId === id) {
                $scope.reports[i] = report;
            }
        }
    };
    $scope.exportData = function (id) {
        $scope.reportedTasks = [];
        for (var i = 0; i < $scope.reports.length; i++) {
            if ($scope.reports[i].reportedSprintsId === id) {
                $scope.reportedTasks = angular.copy($scope.reports[i].reportTasks);
            }
        }

        for (i = 0; i < $scope.reportedTasks.length; i++) {
            $scope.reportedTasks[i].user = $scope.getObjectNameById($scope.reportedTasks[i].user, $scope.users);
            delete $scope.reportedTasks[i].id;
            delete $scope.reportedTasks[i].projectId;
            if ($scope.reportedTasks[i].description === null)
                $scope.reportedTasks[i].description = "No description";
            if ($scope.reportedTasks[i].estimate === null)
                $scope.reportedTasks[i].estimate = "No estimate";
            if (typeof $scope.reportedTasks[i].labels[0] === "undefined")
                $scope.reportedTasks[i].labels = "No label";
            if ($scope.reportedTasks[i].sprintID === null)
                $scope.reportedTasks[i].sprintID = "No sprint";
            else
                $scope.reportedTasks[i].sprintID = $scope.generateSprintName($scope.reportedTasks[i].sprintID);
        }

        var str;
        for (i = 0; i < $scope.reportedTasks.length; i++) {
            str = JSON.stringify($scope.reportedTasks[i]);
            str = str.replace("sprintID", "Sprint Name");
            str = str.replace("name", "Task Name");
            str = str.replace("user", "User Name");
            str = str.replace("description", "Description");
            str = str.replace("state", "State");
            str = str.replace("labels", "Labels");
            str = str.replace("estimate", "Estimate");
            $scope.reportedTasks[i] = JSON.parse(str);
        }


        alasql('SELECT * INTO XLSX("Report to ' + $scope.generateSprintName(id) + '.xlsx",{headers:true}) FROM ?', [$scope.reportedTasks]);
    };

    $scope.getReportDescription = function (id) {
        for (var i = 0; i < $scope.reports.length; i++) {
            if ($scope.reports[i].id === id) {
                $rootScope.description = $scope.reports[i].reportDescription;
            }
        }
    };
});

