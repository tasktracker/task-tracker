angular.module('tttApp').controller('taskCtrl', function ($scope) {
    $scope.setNewTaskObject = function () {
        //set new empty object for modal
        $scope.newTask = {
            "sprintID": null,
            "name": null,
            "user": null,
            "description": null,
            "estimate": null,
            "customLabel": null
        };
    };

    $scope.estimateValidationFail = true;
    $scope.estimateValidation = function (estimate) {
        var taskEstimateFormat;
        if (estimate === undefined || estimate === null) {
            taskEstimateFormat = "Empty spring";

        }
        else {
            var taskEstimateTime = parseInt(estimate);
            taskEstimateFormat = (estimate.replace(taskEstimateTime, '')).toLowerCase();
        }
        switch (taskEstimateFormat) {
            case "h":
                if (taskEstimateTime <= 12 && taskEstimateTime > 0) {
                    return false;
                }
                else {
                    $scope.estimateErrorMessage = 'Task estimate should be >0 hours and <=12 hours';
                    return true;
                }
                break;
            case "d":
                if (taskEstimateTime < 7 && taskEstimateTime > 0) {
                    return false;
                }
                else {
                    $scope.estimateErrorMessage = 'Task estimate should be > 0 days and <7 days';
                    return true;
                }
                break;
            case "w":
                if (taskEstimateTime <= 10 && taskEstimateTime > 0) {
                    return false;
                }
                else {
                    $scope.estimateErrorMessage = 'Task estimate should be > 0 weeks  and <=10 weeks';
                    return true;
                }
                break;
            case "Empty spring":
                return false;
                break;

            default:
            {
                $scope.estimateErrorMessage = 'Enter valid data. Example: 12h or 5w';
                return true;
            }
        }
    };

    $scope.addNewTask = function (sprint, name, user, description, estimate, form) {
        //types convert from <select>
        if (sprint === '' || sprint === null || sprint === undefined)
            sprint = null;
        else sprint = parseInt(sprint);
        if (user === '' || user === null || user === undefined)
            user = null;
        else user = parseInt(user);
        if (description === '' || description === undefined)
            description = null;
        if (estimate === '' || estimate === undefined)
            estimate = null;
        //end of types convert
        var newTask = new Task($scope.tasks.length, sprint, name, user, description, $scope.enteredLabels, $scope.currentProjectID, estimate);
        $scope.tasks.push(newTask);
        //update
        $scope.enteredLabels = [];
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateLocalStorage('tasks');
        form.$setPristine();
        $.simplyToast('New task successfully added', 'success');
    };

    //edit methods
    $scope.setTaskEditFields = function (id) {
        $scope.editedTask = $scope.createCopyOfObjectForEdit(id, $scope.tasks);
        if ($scope.editedTask.estimate === null) $scope.editedTask.estimate = undefined;
    };

    $scope.saveTaskEditChanges = function () {
        //types convert (from <select>)
        if ($scope.editedTask.user === "null" || $scope.editedTask.user === null) $scope.editedTask.user = null;
        else $scope.editedTask.user = parseInt($scope.editedTask.user);
        if ($scope.editedTask.sprintID === "null" || $scope.editedTask.sprintID === null) $scope.editedTask.sprintID = null;
        else $scope.editedTask.sprintID = parseInt($scope.editedTask.sprintID);
        if ($scope.editedTask.estimate === undefined) $scope.editedTask.estimate = null;
        //end of types convert
        //update
        $scope.tasks[$scope.editID] = angular.copy($scope.editedTask);
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateSprints($scope.currentProjectID);
        $scope.updateLocalStorage('tasks');
        $scope.updateLocalStorage('sprints');
        $.simplyToast('Changes saved', 'info');
    };

    //labels methods
    $scope.enteredLabels = [];
    $scope.addLabel = function (label, labels) {
        if (!this.checkLabel(label, labels)) {
            $.simplyToast('Such label exist!', 'danger');
            return;
        }
        labels.push(label);
    };

    $scope.checkLabel = function (label, labels) {
        for (var i = 0; i < labels.length; i++) {
            if (labels[i] === label)
                return false;
        }
        return true;
    };

    $scope.addNewLabelsToNewTask = function (newTaskLabel, labels) {
        if (newTaskLabel) {
            if (!this.checkLabel(newTaskLabel, labels)) {
                $.simplyToast('Such label exist!', 'danger');
                return;
            }
            labels.push(newTaskLabel);
            $scope.newTask.customLabel = null;
        }
        else
            $.simplyToast('Enter valid label`s name', 'danger');
    };

    $scope.deleteLabel = function (index, labels) {
        labels.splice(index, 1);
    };

    //task delete methods
    $scope.setDeleteTask = function (id) {
        $scope.taskToDeleteId = id;
        $scope.deletedTask = $scope.createCopyOfObjectForEdit(id, $scope.tasks);
    };

    $scope.confirmDeletedTask = function () {
        $scope.tasks.splice($scope.taskToDeleteId, 1);
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateLocalStorage("tasks");
        $.simplyToast('Task deleted', 'danger');
    };

    //user methods
    $scope.setUserEditFields = function (id) {
        $scope.editedUser = $scope.createCopyOfObjectForEdit(id, $scope.users);
    };

    $scope.saveUserEditChanges = function () {
        $scope.users[$scope.editID] = angular.copy($scope.editedUser);
        $scope.updateUsers($scope.currentProjectID);
        $scope.updateLocalStorage('users');
        $.simplyToast('Changes saved', 'info');
    };

    $scope.setDeleteUser = function (id) {
        $scope.userToDeleteId = id;
        $scope.userToDelete = $scope.createCopyOfObjectForEdit(id, $scope.users);
    };

    $scope.confirmDeletedUserFromProject = function () {
        for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.userToDeleteId === $scope.users[i].id) {
                for (var j = 0; j < $scope.users[i].projectId.length; j++) {
                    if ($scope.users[i].projectId[j] === $scope.currentProjectID)
                        $scope.users[i].projectId.splice(j, 1);
                }

            }

        }
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateUsers($scope.currentProjectID);
        $scope.updateLocalStorage("tasks");
        $scope.updateLocalStorage("users");
        $.simplyToast('User has been deleted from project', 'warning');
    };

    $scope.confirmDeletedUserCompletelyDelete = function () {

        $scope.users.splice($scope.userToDeleteId, 1);
        $scope.updateTasks($scope.currentProjectID);
        $scope.updateUsers($scope.currentProjectID);
        $scope.updateLocalStorage("tasks");
        $scope.updateLocalStorage("users");
        $.simplyToast('User deleted', 'danger');
    };

    $scope.checkUsersWithoutTasks = function (id) {
        var checked = true;
        for (var i = 0; i < $scope.projectTasks.length; i++) {
            if ($scope.projectTasks[i].user === id) {
                checked = false;
            }
        }
        return checked;
    };

    $scope.createUserReport = function (id) {
        $scope.userReport = [];
        for (var i = 0; i < $scope.projectTasks.length; i++) {
            if ($scope.projectTasks[i].user === id) {
                $scope.userReport.push(angular.copy($scope.projectTasks[i]));
            }
        }

        for (i = 0; i < $scope.userReport.length; i++) {
            delete $scope.userReport[i].id;
            delete $scope.userReport[i].user;
            delete $scope.userReport[i].projectId;
            if ($scope.userReport[i].description === null)
                $scope.userReport[i].description = "No description";
            if ($scope.userReport[i].estimate === null)
                $scope.userReport[i].estimate = "No estimate";
            if (typeof $scope.userReport[i].labels[0] === "undefined")
                $scope.userReport[i].labels = "No label";

            if ($scope.userReport[i].sprintID === null)
                $scope.userReport[i].sprintID = "No sprint";
            else
                $scope.userReport[i].sprintID = $scope.generateSprintName($scope.userReport[i].sprintID);
        }
        var str;
        for (i = 0; i < $scope.userReport.length; i++) {
            str = JSON.stringify($scope.userReport[i]);
            str = str.replace("sprintID", "Sprint Name");
            str = str.replace("name", "Task Name");
            str = str.replace("description", "Description");
            str = str.replace("state", "State");
            str = str.replace("labels", "Labels");
            str = str.replace("estimate", "Estimate");
            $scope.userReport[i] = JSON.parse(str);

            // $scope.userReport[i].Task_Name =  $scope.userReport[i].name;
            // delete $scope.userReport[i].name;
        }
        alasql('SELECT * INTO XLSX("' + $scope.getObjectNameById(id, $scope.users) + ' report.xlsx",{headers:true}) FROM ?', [$scope.userReport]);
    };

});